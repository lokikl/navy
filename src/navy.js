//
// navy
//   - do nothing when requesting url is identical to the current one
//   - default is replacing state
//   - navy-enabled: enable navy for itself and all children
//   - navy-skip: traditional navigation
//   - navy-base: default target
//   - navy-land: add new state
//   - navy-peek: no history and no location changed
//   - navy-scroll-left: auto scroll to target
//   - navy-no-history: no history but has location changed
//   - navy-no-loading-message: omit the "Loading, please wait" message
//   - navy-show-loading-message: show the "Loading, please wait" message when submitting form with navy
//   - data-navy-aimed:
//     jq query, multiple allows, match occurrences one by one, default to body
//     since pesudo-element used in css, so please aim at nodes without before or after
//   - whenever error occurred, alert as this is a design issue
//   - to facilitate the post-redirect-back flow, need to add custom header into the get action
//     rails: response.headers["Current-Url"] = request.path
//
(function(window,undefined){
  $(function(){
    var $body = $('body');
    var loadingMessage = $body.data('navy-loading-message');
    if (!loadingMessage) loadingMessage = "Loading, please wait.";
    var linkTarget = 'a:internal:not(.navy-skip):not([target=_blank])';
    var formTarget = 'form:not(.navy-skip):not([target=_blank])';
    var cachedHTMLs = {};

    function changeDocumentTitle(title) {
      if (title === '') { return; }
      window.document.title = title;
      try {
        var titleHTML = window.document.title
          .replace('<','&lt;')
          .replace('>','&gt;')
          .replace(' & ',' &amp; ');
        window.document.getElementsByTagName('title')[0].innerHTML = titleHTML;
      }
      catch ( Exception ) { }
    }

    // Internal Helper
    // $('a:internal') to select all internal links
    $.expr[':'].internal = function(obj, index, meta, stack){
      var url = $(obj)[0].href || '';
      var rootUrl = window.History.getRootUrl();
      if (url.substring) {
        return url.substring(0,rootUrl.length) === rootUrl;
      } else {
        return false
      }
    };

    function render(context, html, title) {
      var $this = $(this);
      if (context.stack) {
        $(context.aimed + ' div.spinner').remove();
        $(context.aimed).each(function() {
          $this.wrapInner( "<div class='new'></div>");
          var stackContent = $this.data("navy-stack-content") || [];
          stackContent.push($this.clone());
          $this.data("navy-stack-content", stackContent);
        });
      }
      if (context.pop) {
        $(context.pop).each(function() {
          $this = $(this);
          $children = $this.children();
          var $node = $this.data('navy-stack-content').pop().children();
          if (context.popSlide === "slide-backward") {
            $node.css({position: 'absolute', top: 0, width: '100%', right: '-100%'}).appendTo($this).animate({'right': '0%'}, 300);
            $node.animate({'right': '0%'}, 300, function() {
              $node.css({'position': 'relative'});
              $children.remove();
            });
          } else {
            $node.css({position: 'absolute', top: 0, width: '100%', left: '100%'}).appendTo($this).animate({'left': '120%'}, 300);
            $node.animate({'left': '0%'}, 300, function() {
              $node.css({'position': 'relative'});
              $children.remove();
            });
          }
          if ($this.data('navy-stack-content').length === 0) {
            jQuery.removeData($this, "navy-stack-content");
          }
        });
      }
      if (context.animation) {
        $(context.aimed + ' div.spinner').remove();
        $(context.aimed).removeClass('navy-aimed');
      } else {
        $(context.aimed).html('<p>Rendering...</p>');
      }
      var $base = $(html)
      if ($base.filter('.navy-base').length > 0) {
        $base = $base.filter('.navy-base')
      }
      var $newpage = $('<div>')
      $base.appendTo($newpage)

      var $growl = $newpage.find('div.growl');
      if ($growl.length > 0) {
        $('body').append($growl);
      }
      changeDocumentTitle(title);

      var $originals = $(context.aimed);
      var allFound = false;
      $(context.aimed, $newpage).each(function(i) {
        var $node = $(this);
        var $target = $($originals[i]);
        if ($target.length > 0) {
          if (context.animation === 'slide-forward') {
            $node
              .css({position: 'absolute', top: 0, width: '100%', left: '100%'})
              .insertAfter($target)
              .animate({'left': '0%'}, 300);
            $target.animate({'left': '-100%'}, 300, function() {
              $node.css({'position': 'relative'});
              $target.remove();
            });
          } else if (context.animation === 'slide-backward') {
            $node
              .css({position: 'absolute', top: 0, width: '100%', right: '100%'})
              .insertAfter($target)
              .animate({'right': '0%'}, 300);
            $target.animate({'left': '100%'}, 300, function() {
              $node.css({'position': 'relative'});
              $target.remove();
            });
          } else {
            var navyStackContent = $target.data("navy-stack-content");
            $target.replaceWith($node.data("navy-stack-content", navyStackContent));
          }
          allFound = true;
        } else {
          allFound = false;
          return false;
        }
      });
      if (context.requestID !== navyLastRequestID) { context.preventReload = true }
      if (!allFound) {
        if (!context.preventReload) {
          console.log('not all target found, reload the whole page');
          location.reload();
        } else {
          console.log('not all target found, but reload prevented');
        }
      } else {
        if (context.navyScrollLeft) {
          setTimeout(function() {
            var $st = $(context.navyScrollLeft)
            $('html, body').animate({
              scrollLeft: $st.offset().left
            }, 500);
          }, 10);
        } else if (context.aimed === '.navy-base') {
          $('html, body').animate({
            scrollTop: 0,
          }, 1);
        }
      }
    }

    /*
      window.navyRequest({
        // REQUIRED
        url: "/api-path",
        // OPTIONS
        method: 'get',
        data: {},
        preventReload: false,
        action: 'pushState',
        aimed: '.navy-base',
        animation: null, // slide-backward / slide-forward
        stack: false,
        pop: false,
        popSlide: null, // slide-backward / slide-forward
        navyScrollLeft: false,
        navyPeek: false,
      })
    */
    window.navyRequest = function(context) {
      context.requestID = navyLastRequestID = Math.random().toString(36)
      if (context.preventReload === undefined) { context.preventReload = false }
      if (context.action === undefined) { context.action = 'pushState' }
      if (context.aimed === undefined) { context.aimed = '.navy-base' }
      if (context.data === undefined) { context.data = {} }
      if (context.method === undefined) { context.method = 'get' }
      // original href will be treated as a backup plan (nojs set)
      var $all_aimed = $(context.aimed);
      $all_aimed.each(function() {
        var $aimed = $(this);
        $aimed.addClass('navy-aimed');
        if (!context.navyNoLoadingMessage && ($all_aimed.length === 1 || $aimed.width() > 200)) {
          if ($aimed.is(':visible')) {
            var $throbber = $('<div class="spinner"> <div class="bounce1"></div> <div class="bounce2"></div> <div class="bounce3"></div> </div>');
            var offset = $aimed.offset();
            $aimed.append($throbber);
          }
        }
      });
      $.ajax({
        url: context.url,
        method: context.method,
        data: context.data,
        success: function(html, status, xhr){
          html = html.trim();
          try {
            var $page = $(html);
          } catch (e) { }
          if ($page === undefined) {
            $(context.aimed).removeClass('navy-aimed');
            return;
          }
          var $page = $(html);
          var title = $page.filter('title').text();
          if (context.navyPeek) {
            render(context, html, title)
            $(window).trigger('statechangecomplete');
            $("body").trigger('statechanged');
          } else {
            // get url again here for redirection
            var newUrl = xhr.getResponseHeader('Current-Url') || context.url;
            var timestamp = (new Date()).getTime();
            cachedHTMLs[timestamp] = html;
            window.History[context.action]({
              context: context,
              title: title,
              ts: timestamp, // to ensure statechange will be triggered
            }, title, newUrl);
          }
        },
        error: function(xhr, textStatus, errorThrown){
          var errMsg = xhr.responseText;
          console.log('Navy error: ' + context.url + '\n\n' + errMsg);
          $(context.aimed).removeClass('navy-aimed');
        }
      });
    }


    var navyLastRequestID = null; // for determine if reload full page when some target not found
    $body.on('click', linkTarget + '.navy-enabled, .navy-enabled ' + linkTarget, function(event) {
      var $node = $(this);
      // Continue as normal for cmd clicks etc, open new tabs, anchor click...
      if (event.which === 2 || event.metaKey) { return true; }
      if ($node.attr('href')[0] === '#') { return true; }
      event.preventDefault();

      var aimedTarget = $node.data('navy-aimed') || $node.closest('[data-navy-aimed]').data('navy-aimed') || '.navy-base';
      var context = {
        url: $node.attr('href'),
        preventReload: ($node.hasClass('navy-prevent-reload') ? true : false),
        action: ($node.hasClass('navy-peek') ? 'replaceState' : 'pushState'),
        aimed: aimedTarget,
        animation: $node.data('navy-animation'),
        stack: $node.data('navy-stack'),
        pop: $node.data('navy-pop'),
        popSlide: $node.data('navy-pop-slide'),
        navyScrollLeft: $node.data('navy-scroll-left'),
        navyNoLoadingMessage: $node.hasClass('navy-no-loading-message'),
        navyPeek: $node.hasClass('navy-peek'),
      }
      var inBreadcrumb = $node.parents('ol.breadcrumb').length > 0
      if (!context.animation && inBreadcrumb) {
        context.animation = 'slide-backward';
      }
      if ($node.hasClass('navy-no-history')) { context.action = 'replaceState' }
      window.navyRequest(context)
      return false;
    }); // end of the click listener

    $body.on('submit.navy-submit', formTarget + '.navy-enabled, .navy-enabled ' + formTarget, function(event) {
      var $form = $(this);

      if ($form.data('confirm')) {
        if ($.confirm !== undefined) { // jquery-confirm's confirm
          $.confirm({
            title: 'Wait',
            content: $form.data('confirm'),
            buttons: {
              confirm: function () {
                confirmedSubmit($form, event)
              },
              cancel: function () {
              },
            }
          });
        } else { // traditional browser's confirm
          var r = confirm($form.data('confirm'));
          if (r) {
            confirmedSubmit($form, event)
          }
        }
      } else {
        confirmedSubmit($form, event)
      }
      return false;
    }); // end of the form submit listener

    function confirmedSubmit($form, event) {
      if ($form.data('navy-clone')) {
        $('div.cloned', $form).remove()
        var $wrapper = $('<div class="cloned"></div>')
        $($form.data('navy-clone')).clone().appendTo($wrapper)
        $wrapper.appendTo($form)
      }

      var parsley = $form.data('Parsley');
      if (parsley && parsley.isValid() === false) {
        return false;
      }
      $form.find('.codemirror-enabled').each(function() {
        if ($(this).data('codemirror-instance')) {
          $(this).data('codemirror-instance').save();
        }
      });

      navyLastRequestID = Math.random().toString(36)
      var aimedTarget = $form.data('navy-aimed') || $form.closest('[data-navy-aimed]').data('navy-aimed') || '.navy-base';
      var context = {
        requestID: navyLastRequestID,
        action: ($form.hasClass('navy-land') ? 'pushState' : 'replaceState'),
        stackRedirectUrl: $form.data("navy-stack-redirect-url"),
        url: $form.prop('action'),
        aimed: aimedTarget,
        preventReload: ($form.hasClass('navy-prevent-reload') ? true : false),
        animation: $form.data('navy-animation'),
        stack: $form.data('navy-stack'),
        pop: $form.data('navy-pop'),
        popSlide: $form.data('navy-pop-slide'),
      }
      var $aimed = $(context.aimed)
      $aimed.addClass('navy-aimed');
      $aimed.children('button').attr('disabled', 'disabled');
      $aimed.children('a').attr('disabled', 'disabled');
      if ($form.hasClass("navy-show-loading-message")) {
        $aimed
          .attr('aimed-message', loadingMessage)
          .attr('aimed-message-size', 'small');
      }
      if ($form.hasClass("navy-show-throbber")) {
        $aimed.each(function() {
          var $item = $(this);
          if ($item.width() > 200 && $item.is(':visible')) {
            var $throbber = $('<div class="spinner"> <div class="bounce1"></div> <div class="bounce2"></div> <div class="bounce3"></div> </div>');
            var offset = $item.offset();
            $item.append($throbber);
          }
        });
      }
      $form.ajaxSubmit({
        url: context.url,
        data: { "navy_stack_redirect_url" : context.stackRedirectUrl },
        success: function(html, status, xhr) {
          html = html.trim();
          try {
            var $page = $(html);
          } catch (e) { }
          if ($page === undefined) {
            $(context.aimed).removeClass('navy-aimed');
            $(context.aimed).children('button').attr('disabled', '');
            $(context.aimed).children('a').attr('disabled', '');
            return;
          }
          var title = $page.filter('title').text() || $('title').text();
          if ($form.hasClass('navy-peek')) {
            render(context, html, title)
            $(window).trigger('statechangecomplete');
            $("body").trigger('statechanged');
          } else {
            // get url again here for redirection
            var newUrl = xhr.getResponseHeader('Current-Url') || url;
            var timestamp = (new Date()).getTime();
            cachedHTMLs[timestamp] = html;
            window.History[context.action]({
              context: context,
              title: title,
              ts: timestamp, // to ensure statechange will be triggered
            }, title, newUrl);
          }
        },
        error: function(xhr) {
          var errMsg = xhr.responseText;
          // alert(url + '\n\n' + errMsg);
          if (window.growlError) {
            window.growlError(errMsg);
          } else if ($.alert) {
            $.alert({
              title: 'Error',
              content: errMsg,
            });
          }
          $(context.aimed).removeClass('navy-aimed');
        }
      });
    }

    $(window).bind('statechange',function(e) {
      var s = History.getState();
      if (s) {
        var d = s.data;
        if (d && d.title) {
          render(d.context, cachedHTMLs[d.ts], d.title)
          $(window).trigger('statechangecomplete');
          $("body").trigger('statechanged');
        } else {
          if (d.title == "") {
            alert("ERROR: Title tag not found in HTML, may consider adding navy-peek");
          }
          window.location.reload();
        }
      }
    });

  }); // end onDomLoad
})(window);
