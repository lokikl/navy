#!/usr/bin/env bash
set -e

echo "Building navy-builder..."
docker build -t navy-builder ./build

mkdir -p dist
distpath="dist/navy-all-0.0.3.min.js"

echo "Compiling to $distpath"
docker run --rm -v $PWD:$PWD -w $PWD navy-builder \
  src/vendor/history.js \
  src/vendor/history.html4.js \
  src/vendor/history.adapter.jquery.js \
  src/vendor/jquery.form.js \
  src/navy.js \
  > $distpath

