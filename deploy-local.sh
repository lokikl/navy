#!/usr/bin/env bash

echo "Deploy to..."
# find ~/loki ~/appcara -name navy-all.min.js 2>/dev/null | grep -v 'navy/navy-all'
# find ~/loki ~/appcara -name navy-all.min.js 2>/dev/null | grep -v 'navy/navy-all' | xargs -I% cp navy-all.min.js %
find ~/loki ~/appcara -name navy-all-0.0.2.min.js 2>/dev/null | grep -v 'navy/dist'

# find ~/loki ~/appcara -name navy-all.0.0.1.min.js 2>/dev/null | grep -v 'navy/navy-all'
# find ~/loki ~/appcara -name navy-all.0.0.1.min.js 2>/dev/null | grep -v 'navy/navy-all' | xargs -I% cp navy-all.min.js %
find ~/loki ~/appcara -name navy-all-0.0.2.min.js 2>/dev/null | grep -v 'navy/dist' | xargs -I% cp dist/navy-all-0.0.2.min.js %
